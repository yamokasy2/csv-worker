## Project Test task

### Guide

! check you .env.example. Please using CLIENT_USERNAME CLIENT_PASSWORD in env or update data in vars(it's for Postman)
 
1. set .env.example to .env
2. ```docker-compose up```
3. ```docker-compose exec app bash``` (CLI)
4. in CLI: ```composer install```
5. in CLI: ```php artisan migrate```
6. in CLI: ```php artisan queue:listen --queue=sync-clients --timeout=1000 --tries=1```
7. Postman requests: https://www.postman.com/glurk/workspace/test-task/collection/1073851-eb18b097-eb31-49ca-90c7-f1eb0225ce27?action=share&creator=1073851
    7.1 Or u can import postman collection project: [task.postman_collection.json](task.postman_collection.json)
8. Call [https://www.postman.com/glurk/workspace/test-task/request/1073851-ee3a8bc9-eb7b-4b52-9035-e50f13bc8726](Add file)
    8.1 Required! Select file for parse. When action add file you see work queue in CLI(import data to DB)
9. When import was finish, u can use [https://www.postman.com/glurk/workspace/test-task/request/1073851-044f8218-5555-4292-abe8-8fb6ce117888](clients)


If u have problem with big file. U can parse file in cli
1. Upload file to /storage/app/public/import/(filename)
2. ```php artisan clients:parse --file=(filename)```

But firstly, check pls .env need: QUEUE_CONNECTION=database (after ```php artisan migrate```)


### Requirements
- Docker: 4.10.1
