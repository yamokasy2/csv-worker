<?php

namespace App\Services;

use App\Filters\ClientFilter;
use App\Jobs\SyncClientsJob;
use App\Models\Client;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Cache;

/**
 *
 */
class ClientService
{

    /**
     * @var int
     */
    protected int $itemSize = 25;

    /**
     * @param Client $model
     */
    public function __construct(protected Client $model)
    {
    }

    /**
     * @param ClientFilter $filter
     * @return LengthAwarePaginator
     */
    public function getClientsWithPaginate(ClientFilter $filter): LengthAwarePaginator
    {
        return $this->getClients($filter)->paginate($this->itemSize);
    }

    /**
     * @param Request $request
     * @return void
     */
    public function syncClients(Request $request): void
    {
        $filename = $this->createFile($request->file('data-file'));
        Cache::put('status-store', true, Carbon::now()->addMinutes(10));

        $this->dispatchJob($filename, $request);
    }

    /**
     * @param string $filename
     * @return void
     */
    public function parseLocalFile(string $filename)
    {
        SyncClientsJob::dispatch($filename)->onQueue('sync-clients');
//        dispatch(new SyncClientsJob($filename))->onQueue('sync-clients');
    }

    /**
     * @param string $filename
     * @param Request $request
     * @return void
     */
    protected function dispatchJob(string $filename, Request $request): void
    {
        dispatch(
            new SyncClientsJob($filename, $request->separate, $request->enclosure, $request->escape)
        )->onQueue('sync-clients');
    }

    /**
     * @param UploadedFile $file
     * @param string $extension
     * @return string
     */
    protected function createFile(UploadedFile $file, string $extension = 'csv'): string
    {
        $ds = DIRECTORY_SEPARATOR;
        $path = storage_path('app' . $ds . 'public' . $ds . 'uploads');
        $filename = 'data-file.' . $extension;
        $file->move($path, $filename);

        return $path . $ds . $filename;
    }


    /**
     * @param ClientFilter $filter
     * @return Builder
     */
    protected function getClients(ClientFilter $filter): Builder
    {
        return Client::filter($filter)->with(['category']);
    }
}
