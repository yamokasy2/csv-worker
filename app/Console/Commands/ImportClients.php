<?php

namespace App\Console\Commands;

use App\Services\ClientService;
use Illuminate\Console\Command;
use Illuminate\Container\Container;

class ImportClients extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'clients:parse {-f|--file=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Parse and sync clients local file';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $filename = storage_path(sprintf('app/public/import/%s',$this->option('file')));
        if(!file_exists($filename)) throw new \ErrorException("File not found path: ".$filename, 400);

        /** @var ClientService $service */
        $service = Container::getInstance()->make(ClientService::class);
        $service->parseLocalFile($filename);

        $this->info("Job added!");

        return true;
    }
}
