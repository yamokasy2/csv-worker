<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpFoundation\Response;

class ValidClientBaseMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        if($request->getUser() != env('CLIENT_USERNAME') || $request->getPassword() != env('CLIENT_PASSWORD'))
            return new JsonResponse(['status' => false, 'payload' => ['msg' => 'Invalid credentials.']], 401, ['WWW-Authenticate' => 'Basic']);

        return $next($request);
    }
}
