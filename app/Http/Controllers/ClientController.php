<?php

namespace App\Http\Controllers;

use App\Filters\ClientFilter;
use App\Http\Resources\ClientResource;
use App\Models\Client;
use App\Services\ClientService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

/**
 *
 */
class ClientController extends Controller
{
    /**
     * @param ClientService $service
     */
    public function __construct(protected ClientService $service)
    {
    }

    /**
     * Display a listing of the resource.
     */
    public function index(ClientFilter $request)
    {
        return ClientResource::collection($this->service->getClientsWithPaginate($request));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request): JsonResponse
    {
        if (Cache::has('status-store'))
            return new JsonResponse(['status' => true, 'payload' => ['message' => 'old working does a finish. Please wait']], 200);

        $this->service->syncClients($request);

        return new JsonResponse(['status' => true, ['payload' => ['message' => 'OK!']]], 203);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id): ClientResource
    {
        return new ClientResource(Client::findOrFail($id));
    }
}
