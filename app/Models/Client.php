<?php

namespace App\Models;

use App\Filters\QueryFilter;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 *
 */
class Client extends Model
{
    use HasFactory;

    /**
     * @var string
     */
    protected $table = 'clients';

    /**
     * @var string[]
     */
    protected $fillable = [
        'firstname',
        'lastname',
        'email',
        'gender',
        'birthDate'
    ];

    /**
     * @return BelongsTo
     */
    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * @param Builder $build
     * @param QueryFilter $queryFilter
     * @return Builder
     */
    public function scopeFilter(Builder $build, QueryFilter $queryFilter): Builder
    {
        return $queryFilter->apply($build);
    }
}
