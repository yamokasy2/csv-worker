<?php

namespace App\Jobs;

use App\Models\Category;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;
use Throwable;

/**
 *
 */
class SyncClientsJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var string
     */
    protected string $filename;

    /**
     * @var string|mixed
     */
    protected string $separate;

    /**
     * @var string|mixed
     */
    protected string $enclosure;

    /**
     * @var string|mixed
     */
    protected string $escape;

    /**
     * Create a new job instance.
     */
    public function __construct($filename, $separate = ',', $enclosure = "\"", $escape = '\\')
    {
        $this->filename = $filename;
        $this->separate = $separate;
        $this->enclosure = $enclosure;
        $this->escape = $escape;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        $collect = $this->getParseData();
        $itemsByCategory = $collect->groupBy('category');
        $categories = $this->syncCategories($itemsByCategory->keys());

        $categories->map(function ($item) use ($itemsByCategory) {
            $data = $itemsByCategory[$item->name];

            $item->clients()->createMany($data);// I dont know who unique. Email duplicates... @Todo send question about unique attr
        });

        Cache::forget('status-store');
        if (Storage::exists($this->filename))
            Storage::delete($this->filename);
    }

    /**
     * @return Collection
     */
    protected function getParseData(): Collection
    {
        $rows = [];
        if (($handle = fopen($this->filename, "r")) !== FALSE) {
            $header = fgetcsv($handle);
            while ($row = fgetcsv($handle)) {
                $item = array_combine($header, $row);
                $rows[] = $item;
            }

            fclose($handle);
        }

        return Collection::make($rows);
    }

    /**
     * @param Collection $collection
     * @return Collection
     */
    protected function syncCategories(Collection $collection): Collection
    {
        return $collection->map(function ($name) {
            return Category::firstOrCreate(['name' => $name]);
        });
    }

    /**
     * @param Throwable $exception
     * @return void
     */
    public function failed(Throwable $exception): void
    {
        Cache::forget('status-store');
    }
}
