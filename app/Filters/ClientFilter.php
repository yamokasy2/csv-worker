<?php

namespace App\Filters;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Carbon;

/**
 *
 */
class ClientFilter extends QueryFilter
{
    /**
     * @param $name
     * @return Builder
     */
    public function category($name): Builder
    {
        return $this->builder->join('categories', 'categories.id', '=', 'clients.category_id')
            ->where('categories.name', $name);
    }

    /**
     * @param $name
     * @return Builder
     */
    public function gender($name): Builder
    {
        return $this->builder->where('gender', $name);
    }

    /**
     * @param $date
     * @return Builder
     */
    public function birthDate($date): Builder
    {
        return $this->builder->where('birthDate', $date);
    }

    /**
     * @param int $age
     * @return Builder
     */
    public function age(int $age): Builder
    {
        return $this->builder->whereBetween('birthDate', [Carbon::now()->subYears($age)->format('Y-m-d'), Carbon::now()->format('Y-m-d')]);
    }

    /**
     * @param string $range
     * @return Builder
     */
    public function ageRange(string $range): Builder
    {
        $exYears = explode('-', $range);

        return $this->builder->whereBetween('birthDate', [Carbon::now()->subYears(max($exYears))->format('Y-m-d'), Carbon::now()->subYears(min($exYears))->format('Y-m-d')]);
    }

}
